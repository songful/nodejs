/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var about = require('./routes/about');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var lessMiddleware = require('less-middleware');


var app = express();
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(flash());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.session({secret: "123456"}));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(lessMiddleware({
    src: __dirname + '/public',
    compress: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

mongoose.connect('mongodb://localhost/todo_development', function (err) {
    if (!err) {
        console.log('connectted to mongodb');
    } else {
        console.log('can not connectted to mongodb');
        throw err;
    }
});
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Task = new Schema({
    task: String
});
var Tasks = mongoose.model('Task', Task);

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/about', about.about);
app.post('/add', user.post);
app.get('/users/:id', user.id);
app.get('/users/:id/:detail', user.detail);

app.get('/tasks', function (req, res) {
    Tasks.find({}, function (err, docs) {
        res.render('tasks/index', {
            title: 'Todos index view',
            docs: docs
        });
    });
});

app.get('/tasks/new', function (req, res) {
    res.render('tasks/new', {title: 'New Task'});
});

app.post('/tasks/add', function (req, res) {
    var task = new Tasks(req.body.task);
    task.save(function (err) {
        if (!err) {
            req.flash('info', 'It worked!');
            res.redirect('/tasks');
        } else {
            req.flash('warning', 'Error!');
            res.redirect('/tasks/new');
        }
    });
});

app.get('/tasks/:id/edit', function (req, res) {
    Tasks.findById(req.params.id, function (err, doc) {
        res.render('tasks/edit', {
            title: "Edit task view",
            task: doc
        });
    });
});
app.put('/tasks/:id', function (req, res) {
    Tasks.findById(req.params.id, function (err, doc) {
        doc.task = req.body.task.task;
        doc.save(function (err) {
            if (!err) {
                res.redirect("/tasks");
            }
        });
    });
});

app.del('/tasks/:id', function (req, res) {
    Tasks.findById(req.params.id, function (err, doc) {
        if (!doc) {
            return next(new NotFound('Document not found'));
        } else {
            doc.remove(function () {
                req.flash("info", "success");
                res.redirect('/tasks');
            });
        }
    });
});
/*
 app.get('*', function(req, res) {
 //console.log('404 handler..')
 res.render('404', {
 status: 404,
 title: 'NodeBlog',
 });
 });*/

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
