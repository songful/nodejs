
/*
 * GET users listing.
 */

exports.list = function(req, res){
  res.send("respond with a resource");
};
exports.post = function(req, res){
    res.send(req.body);
};
exports.id = function(req, res){
    res.send('show content for user id: ' + req.params.id);
};
exports.detail = function(req, res){
    res.send('show content for user id: ' + req.params.id + '\ndeatail: ' + req.params.detail);
};